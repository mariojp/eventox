<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>


<head>
<jsp:useBean id="lista" class="java.util.ArrayList" scope="application"></jsp:useBean>

<style type="text/css">
p.ex1 {
	padding-top: 625px;
}

html, body {
	height: 100%;
}

html {
	display: table;
	margin: auto;
}

body {
	display: table-cell;
}
</style>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Eventos</title>
</head>

<body>
	<h1>EventoX</h1>
	<div>
	<center>
	<div class="input-group mb-3">
		<div class="input-group-prepend">
			<span class="input-group-text" id="basic-addon1"> <img
				width="20" height="20"
				src="https://icon-icons.com/icons2/1111/PNG/512/loupe_79257.png">
			</span>
		</div>
		<input type="text" class="form-control" name="pesquisa"
			placeholder="Digite aqui..." size="65">
		<form action="/TQ/PesquisarController" method="post">
			<div class="input-group-append">
				<button class="btn btn-outline-secondary" type="submit"
					id="button-addon2">Pesquisar</button>
			</div>
		</form>
		<br> <a href="/TQ/CadastrarEvento.jsp">Cadastrar Evento</a> <br>
	</center>
	</div>
	<div>
		<table>
			<tr>
				<th>Nome:</th>
				<th>Local:</th>
				<th>Data:</th>
				<th>Descricao:</th>
				<th></th>

			</tr>
			<c:forEach items="${lista}" var="evento">
				<tr>
					<td>${evento.nome}</td>
					<td>${evento.local}</td>
					<td>${evento.data}</td>
					<td>${descricao}</td>
				</tr>
			</c:forEach>
		</table>
		</div>
</body>

</html>