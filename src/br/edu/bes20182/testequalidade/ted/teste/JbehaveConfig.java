package br.edu.bes20182.testequalidade.ted.teste;

public class JbehaveConfig extends JUnitStories{
	@SuppressWarnings("deprecation")
	public Configuration configuration() {

		Keywords keywords = new LocalizedKeywords(new Locale("pt"));

		return new MostUsefulConfiguration().useKeywords(keywords).useStoryParser(new RegexStoryParser(keywords))
				.useStoryLoader(new LoadFromClasspath(this.getClass())).useStoryReporterBuilder(
						new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE, Format.HTML));
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new ContaCorrenteSteps());
	}

	@Override
	protected List<String> storyPaths() {
		return Arrays.asList("br/ucsal/bes20182/testequalidade/ted/teste/jbehaveStory.story");
	}

}
