package br.edu.bes20182.testequalidade.ted.teste;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

static WebDriver driverChrome;


public class SeleniumTeste {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:/chromedriver/chromedriver.exe");

		 driverChrome = new ChromeDriver();
		 driverChrome.get("file:///C:/Users/rodi/git/eventox/WebContent/Main.jsp");

	
		
		int esperado = 1;
		@Given("Dado que abrir a pagina principal")
		public void paginaPrincipal() {
			
		}
		
		@When("Quando realizo uma pesquisa por $pesquisa")
		public void realizarPesquisa(String pesquisa) {
			WebElement produto = driverChrome.findElement(By.id("pesquisa"));
			produto.sendKeys(pesquisa);
		}
		@Then("Ent�o o resultado ser� 1")
		WebElement button = driverChrome.findElement(By.id("button-addon2"));
		button.click();
		
		Assert.assertEquals(esperado, button);
		

	@AfterClass
	public static void setUpAfterClass() throws Exception {

		 driverChrome.close();
		 driverChrome.quit();
	}


}
