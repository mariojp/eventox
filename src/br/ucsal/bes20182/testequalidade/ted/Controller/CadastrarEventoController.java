package br.ucsal.bes20182.testequalidade.ted.Controller;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucsal.bes20182.testequalidade.ted.DAO.EventoDAO;
import br.ucsal.bes20182.testequalidade.ted.Main.Evento;

@WebServlet("/CadastrarEventoController")
public class CadastrarEventoController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CadastrarEventoController() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Evento evento = new Evento();
		EventoDAO eventoDAO = new EventoDAO();

		String nome = request.getParameter("nome");
		String local = request.getParameter("local");
		String descricao = request.getParameter("descricao");
		String data = request.getParameter("data");

		evento.setNome(nome);
		evento.setLocal(local);
		evento.setDescricao(descricao);

		try {
			evento.setData(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		eventoDAO.cadastrarEvento(evento);
		System.out.println("Cadastro com sucesso!");
		response.sendRedirect("/TQ/Eventos.jsp");
	}

}
