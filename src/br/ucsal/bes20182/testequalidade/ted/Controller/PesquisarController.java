package br.ucsal.bes20182.testequalidade.ted.Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucsal.bes20182.testequalidade.ted.DAO.EventoDAO;
import br.ucsal.bes20182.testequalidade.ted.Main.Evento;

/**
 * Servlet implementation class PesquisarController
 */
@WebServlet("/PesquisarController")
public class PesquisarController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public PesquisarController() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("ENTROU NO PRESQUISAR CONTROLLER");
		List<Evento> eventos = (List) request.getServletContext().getAttribute("lista");
		EventoDAO eventoDAO = new EventoDAO();
		String pesquisa = request.getParameter("pesquisa");
		eventos = eventoDAO.pesquisarEvento(pesquisa);
		request.setAttribute("lista", eventos);
		eventoDAO.retornoEvento(eventos);
		response.sendRedirect("Eventos.jsp");
	}
}
