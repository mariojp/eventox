package br.ucsal.bes20182.testequalidade.ted.DAO;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.testequalidade.ted.Main.Evento;

public class EventoDAO {
	static List<Evento> eventos = new ArrayList<Evento>();
	static List<Evento> resultadoPesquisa = new ArrayList<Evento>();

	public void cadastrarEvento(Evento evento) {
		eventos.add(evento);
	}
	
	public void retornoEvento(List<Evento> eventos) {
		for (int i = 0; i < eventos.size(); i++) {
			System.out.println(eventos.get(i).getNome());
		}
	}

	public void removerEvento(Evento evento) {
		eventos.remove(evento);
	}

	static public List<Evento> pesquisarEvento(String pesquisa) {
		for (int i = 0; i < eventos.size(); i++) {
			if (eventos.get(i).getNome().equals((pesquisa))) {
				resultadoPesquisa.add(eventos.get(i));
			}
		}
		return resultadoPesquisa;
	}

}
