package br.ucsal.bes20182.testequalidade.ted.Main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Evento {

	private String nome;
	private String local;
	private Date data;
	private String descricao;

	public Evento(String nome, String local, Date data, String descricao) {
		this.nome = nome;
		this.local = local;
		this.data = data;
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public Date getData() {
		return data;
	}

	public void setData(String data) throws ParseException {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date dataFormatada = formato.parse(data);
		this.data = dataFormatada;
	}

}
